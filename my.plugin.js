/*
 * @author Marouan KHIARI <marwen.khyari@gmail.com>
 */

function $() {
    if ((typeof arguments[0]) === 'string' && arguments[0].length > 0) {
        return document.querySelector(arguments[0]);
    }
}
let $$ = (function ($) {
    const _isId = (str) => {
        if ((typeof str) === 'string' && str.match('#.+')) {
            return true;
        }
        return false;
    }

    const _isClass = (str) => {
        if ((typeof str) === 'string' && str.match('..+')) {
            return true;
        }
        return false;
    }

    $.prototype.css = function () {
        if (typeof arguments === 'object' && arguments !== null) {
            if ((typeof arguments[0]) === 'string') {
                this.style[arguments[0]] = arguments[1];
            } else if (arguments.length === 1) {
                for (const [key, value] of Object.entries(arguments[0])) {
                    //console.log(`${key}: ${value}`);
                    this.style[key] = value;
                }
            }
        }
    }

    $.prototype.wrap = function () {
        console.log(typeof arguments[0]);
        if (typeof arguments === 'object' && arguments !== null) {
            if ((typeof arguments[0]) === 'string' && arguments[0].length > 0) {
                let wrapper = document.createElement(arguments[0]);
                if (_isId(arguments[1])) {
                    wrapper.setAttribute('id', arguments[1].substr(1));
                } else if (_isClass(arguments[1])) {
                    wrapper.setAttribute('class', arguments[1].substr(1));
                }
                this.parentNode.insertBefore(wrapper, this);
                wrapper.appendChild(this);
            }
        }
    }

    $.prototype.count = function () {
        return arguments.length;
    }

}(Object));
